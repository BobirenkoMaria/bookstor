<div class="ht__bradcaump__area {{ $bg }}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="bradcaump__inner text-center">
                    <h2 class="bradcaump-title">{{ $title }}</h2>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>