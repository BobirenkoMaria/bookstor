<div class="fixed-bottom p-4">
    <div class="cookie-notification toast bg-dark text-white w-100 mw-100" role="alert" data-autohide="false" style="display: none">
        <div class="toast-body p-4 d-flex flex-column">
            <h3>Уведомление Cookie</h4>
            <p style="font-size: 20px">
            Продолжая пользоваться этим сайтом вы даете подтверждение на использование файлов cookie
            </p>
            <div class="ml-auto">
                <button type="button" class="btn btn-light" id="btnAccept">
                    Отлично
                </button>
            </div>
        </div>
    </div>
</div>