<header id="wn__header" class="header__area header__absolute sticky__header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-6 col-lg-2">
                <div class="logo">
                    <a href={{ route('home') }}>
                        <img src="/images/logo/logo.png" alt="logo images">
                    </a>
                </div>
            </div>
           <div class="col-lg-4 col-sm-12 d-none d-lg-block">
               <nav class="mainmenu__nav">
                   <ul class="meninmenu d-flex justify-content-start">
                       <li class=@if (str_contains(Route::currentRouteName(), 'home')) active_page @endif>
                           <a href={{ route('home') }}>Главная</a>
                       </li>
                       <li class=@if (Route::currentRouteName() === 'about') active_page @endif>
                           <a href={{ route('about') }}>О нас</a>
                       </li>
                       <li class=@if (Route::currentRouteName() === 'account') active_page @endif>
                           <a href={{ route('account') }}>Личный кабинет</a>
                       </li>
                   </ul>
               </nav>
           </div>
            <div class="col-md-8 col-lg-6 col-5 col-lg-2">
                <div class="search__box">
                    <form method="get" action={{ route('home') }}>
                        <div class="form__box">
                            <input type="text" name="search" placeholder="Поиск в Bookstor" value="{{ request()->get('search') }}">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Start Mobile Menu -->
        <div class="row d-none">
            <div class="col-lg-12 d-none">
                <nav class="mobilemenu__nav">
                    <ul class="meninmenu">
                        <li><a href={{ route('home') }}>Главная</a></li>
                        <li><a href={{ route('about') }}>О нас</a></li>
                    </ul>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- End Mobile Menu -->
        <div class="mobile-menu d-block d-lg-none">
        </div>
        <!-- Mobile Menu -->	
    </div>		
</header>