<!-- Start Error Area -->
<section class="page_error pt--120 pb--55 bg--white">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="error__inner text-center">
                    <div class="error__logo">
                        <a href={{ route('home') }}><img src={{ $picture }} alt="error images"></a>
                    </div>
                    <div class="error__content">
                        <h2>{{ $message }}</h2>
                        <p>{{ $description }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Error Area -->