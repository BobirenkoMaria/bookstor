<footer id="wn__footer" class="footer__area bg__cat--8 brown--color">
    <div class="footer-static-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer__widget footer__menu">
                        <div class="ft__logo">
                            <a href={{ route('home') }}>
                                <img src="/images/logo/3.png" alt="logo">
                            </a>
                            <p>Добро пожаловать в наш магазин. Надеемся, вы найдете то, что вам нужно!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>