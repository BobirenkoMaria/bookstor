<!doctype html>
<html class="no-js" lang="zxx">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>@yield('title') | Bookstor</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Favicons -->
	<link rel="shortcut icon" href="/images/favicon.ico">
	<link rel="apple-touch-icon" href="/images/icon.png">

	<!-- Google font (font-family: 'Roboto', sans-serif; Poppins ; Satisfy) -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,600,600i,700,700i,800" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet"> 

	<!-- Stylesheets -->
	@vite(['resources/css/bootstrap.min.css'])
	@vite(['resources/css/plugins.css'])
	@vite(['resources/css/style.css'])

	<!-- Cusom css -->
   @vite(['resources/css/custom.css'])

	<!-- Modernizer js -->
	@vite(['resources/js/vendor/modernizr-3.5.0.min.js'])
</head>
<body>
	<!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

	<!-- Main wrapper -->
	<div class="wrapper" id="wrapper">
		<!-- Header -->
		@include('layouts.header')
		<!-- //Header -->
		<!-- Start Search Popup -->
		@include('layouts.search')
		<!-- End Search Popup -->
		<!-- Start Bradcaump area -->
        @yield('bradcaump')
        <!-- End Bradcaump area -->
		<!-- Start Main Content Wrapper -->
		@yield('content')
		<!-- End Main Content Wrapper -->
		<!-- Footer Area -->
		@include('layouts.footer')
		<!-- //Footer Area -->
		<!-- Cookie area -->
		@include('layouts.cookie')
		<!-- //Cookie area -->
	</div>
	<!-- //Main wrapper -->

	<!-- JS Files -->
	@vite(['resources/js/vendor/jquery-3.2.1.min.js'])
	@vite(['resources/js/popper.min.js'])
	@vite(['resources/js/bootstrap.min.js'])
	@vite(['resources/js/plugins.js'])
	@vite(['resources/js/active.js'])
	<script src="https://kit.fontawesome.com/6b773c9e6a.js" crossorigin="anonymous"></script>

</body>
</html>