<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email Verification</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            line-height: 1.6;
            color: #333;
        }

        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
            background-color: #f9f9f9;
        }

        h1 {
            font-size: 24px;
            margin-bottom: 20px;
        }

        p {
            margin-bottom: 20px;
        }

        a {
            display: inline-block;
            padding: 10px 20px;
            background-color: #ffffff;
            color: #000000;
            text-decoration: none;
            border-radius: 5px;
        }

        a:hover {
            background-color: #c9e3ff;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Верификация Email</h1>
        <p>Для продолжения покупки книги, пожалуйста, перейдите по ссылке:</p>
        <a href="{{ $verificationLink }}">Подтвердить email</a>
        <p>Как только оплата пройдет, вам придет письмо с вашей книгой</p>
    </div>
</body>
</html>
