@extends('app')

@section('title', 'Заказ ' . $book->name)

@section('content')
<!-- Start Checkout Area -->
<section class="wn__checkout__area pt--120 pb--55 bg__white">
    <div class="container">
        <form action='/book/{{ $book->id }}/verificated-order?email={{ $email }}' method="POST">
            @csrf
            <div class="row">
                <div class="col-lg-6 col-12">
                    <div class="customer_details">
                        <h3>Детали заказа</h3>
                        <div class="customar__field">
                            <div class="customar__field">
                                <div class="input_box">
                                    <label>Email</label>
                                    <input type="text" value="{{ $email }}" readonly name="email">
                                </div>
                                <div class="input_box">
                                    <label>Держатель карты (латинскими буквами) <span>*</span></label>
                                    <input type="text" name="cc-name">
                                </div>
                                <div class="input_box">
                                    <label>Номер карты <span>*</span></label>
                                    <input type="text" name="cc-number">
                                </div>
                                <div class="margin_between">
                                    <div class="input_box space_between">
                                        <label>Действует до <span>*</span></label>
                                        <input type="text" name="cc-exp">
                                    </div>
                                    <div class="input_box space_between">
                                        <label>CVC/CVV код <span>*</span></label>
                                        <input type="text" name="cc-csc">
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12 md-mt-40 sm-mt-40">
                    <div class="wn__order__box">
                        <h3 class="onder__title">Ваш заказ</h3>
                        <ul class="order__total">
                            <li>Книга</li>
                            <li>Цена</li>
                        </ul>
                        <ul class="order_product">
                            <li>{{ $book->name }} <span>{{ $book->price }} ₽</span></li>
                        </ul>
                        <button class="buy--btn" type="submit">Оформить заказ</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- End Checkout Area -->
@endsection