@extends('app')

@section('title', $book->name)

@section('content')
<div class="maincontent bg--white pt--120 pb--55">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-12">
                <div class="wn__single__product">
                    <div class="row">
                        <div class="col-lg-6 col-12">
                            <div class="wn__fotorama__wrapper">
                                <div class="fotorama wn__fotorama__action" data-nav="thumbs">
                                      <a><img src={{ $book->picture }} alt=""></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="product__info__main">
                                <h1>{{ $book->name }}</h1>
                                <div class="product-reviews-summary d-flex">
                                    <ul class="rating d-flex">
                                        @foreach (range(1, 5) as $star)
                                            <li class=@if ($book->rate >= $star) on @endif><i class="fa-solid fa-star"></i></li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="price-box">
                                    <span>{{ $book->price }} ₽</span>
                                </div>
                                <div class="box-tocart d-flex">
                                    <form class="addtocart__actions" action="/book/{{ $book->id }}/pre-order" method="POST">
                                        @csrf
                                        <div class="input_box">
                                            <input type="email" required name="email" placeholder="Email*">
                                        </div>
                                        <button class="tocart" type="submit">Купить</button>
                                    </form>
                                </div>
                                <div class="product__overview">
                                    <p>{{ $book->description }}</p>
                                    <ul class="pro__attribute">
                                        <li><b>Авторы</b></li>
                                        <li>
                                            @if (count($book->authors) == 0) Нет авторов @endif
                                            @foreach ($book->authors as $id => $author)
                                                <span class="tag">{{ $author->name }}</span>&nbsp;
                                            @endforeach
                                        </li>
                                    </ul>
                                    <ul class="pro__attribute">
                                        <li><b>Жанры</b></li>
                                        <li>
                                        @if (count($book->genres) == 0) Нет жанров @endif
                                        @foreach ($book->genres as $id => $genre)
                                            <span class="tag">{{ $genre->name }}</span>&nbsp;
                                        @endforeach
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection