@extends('app')

@section('title', 'Успешная покупка ' . $book->name)

@section('bradcaump')
@include('layouts.breadcrumb', ['title' => 'Успешная покупка', 'bg' => 'bg-image--1'])
@endsection

@section('content')
<div class="page-about about_area bg--white pt--120 pb--55">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title--3 text-center pb--30">
                    <h2>Спасибо за покупку книги <b>"{{ $book->name }}"</b> на нашем сайте!</h2>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection