@extends('app')

@section('title', __('Server Error'))

@section('content')
    @include('layouts.error', [
        'picture' => '/images/others/500.png',
        'message' => 'Ошибка - сервер',
        'description' => 'Наши администраторы уже занимаются решением данной проблемы',
    ])
@endsection
