@extends('app')

@section('title', __('Not Found'))

@section('content')
    @include('layouts.error', [
        'picture' => '/images/others/404.png',
        'message' => 'Ошибка - не найдено',
        'description' => 'Похоже, что вы перешли по неверному адресу. Попробуйте вернуться на главную страницу',
    ])
@endsection