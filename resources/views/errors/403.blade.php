@extends('app')

@section('title', __('Forbidden'))
@section('content')
    @php
        if(!isset($message)) $message = 'Ошибка - нет доступа';
    @endphp

    @include('layouts.error', [
        'picture' => '/images/others/403.png',
        'message' => $message,
        'description' => 'У вас нет доступа к этой странице',
    ])
@endsection