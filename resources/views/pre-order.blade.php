@extends('app')

@section('title', 'Верифицируйте email')

@section('bradcaump')
@include('layouts.breadcrumb', ['title' => 'Успешная покупка', 'bg' => 'bg-image--1'])
@endsection

@section('content')
<div class="page-about about_area bg--white pt--120 pb--55">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title--3 text-center pb--30">
                    <h2>Совсем скоро письмо с информацией об оплате <b>"{{ $book->name }}"</b> окажется на {{ $email }}</h2>
                    <p>Вы можете закрыть эту вкладку</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection