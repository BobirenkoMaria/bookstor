@extends('app')

@section('title', 'О нас')

@section('bradcaump')
@include('layouts.breadcrumb', ['title' => 'О нас', 'bg' => 'bg-image--1'])
@endsection

@section('content')
<!-- Start About Area -->
<div class="page-about about_area bg--white section-padding--sm">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title--3 text-center pb--30">
                    <h2>Книги на Bookstor: огромный выбор, доступные цены и удобство покупок</h2>
                </div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="content">
                <p class="mt--20 mb--20">Добро пожаловать в наш интернет-магазин! Здесь вы найдёте книги разных жанров, направлений и авторов. Мы предлагаем доступные цены, удобные условия оплаты и быструю доставку вашей покупки на почту.
                    <br>Мы постоянно обновляем ассортимент и следим за новинками литературы, чтобы предложить вам самые актуальные и востребованные издания.
                    <br>Мы рады видеть вас среди наших постоянных покупателей и надеемся, что наш интернет-магазин станет вашим любимым местом для покупки книг в электронном формате!
                </p>
            </div>
        </div>
        <section class="wn_contact_area bg--white pt--80">
        	<div class="container">
        		<div class="row">
        			<div class="col-lg-8 col-12">
        				<div class="contact-form-wrap">
        					<h2 class="contact__title">Чего-то не хватает?</h2>
        					<p class="contact__desc">Напишите нам, если у вас остались вопросы или вы не нашли книгу, которую ищете</p>
                            <form action={{ route('contact') }} method="post">
                                @csrf
                                <div class="single-contact-form space-between">
                                    <input type="text" required name="name" placeholder="Ваше имя">
                                    <input type="email" required name="email" placeholder="Ваш Email">
                                </div>
                                <div class="single-contact-form message">
                                    <textarea name="message" required placeholder="Ваше сообщение"></textarea>
                                </div>
                                <div class="contact-btn">
                                    <button type="submit">Отправить</button>
                                </div>
                            </form>
                        </div>
                        @if (Session::has('success'))
                            <div class="form-output pt--30">
                                <p class="form-messege">{{ Session::get('success') }}</p>
                            </div>
                        @endif
        			</div>
        			<div class="col-lg-4 col-12 md-mt-40 sm-mt-40">
        				<div class="wn__address">
        					<h2 class="contact__title">Наши контакты</h2>
        					<p class="contact__desc">Или же позвоните нам на номера</p>
        					<div class="wn__addres__wreapper">
        						<div class="single__address">
        							<i class="fa-solid fa-phone"></i>
        							<div class="content">
        								<span>Дмитрий Олегович</span>
        								<p>+7 (914) 336-60-64</p>
        							</div>
        						</div>
                                <div class="single__address">
        							<i class="fa-solid fa-phone"></i>
        							<div class="content">
        								<span>Мария Викторовна</span>
        								<p>+7 (999) 059-82-43</p>
        							</div>
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
    </div>
</div>
<!-- End About Area -->
@endsection