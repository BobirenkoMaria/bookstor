@extends('app')

@section('title', 'Главная')

@section('bradcaump')
@include('layouts.breadcrumb', [
    'title' => 'Главная',
    'bg' => 'bg-image--1',
    ])
@endsection

@section('content')
<section class="wn__bestseller__area bg--white  pb--30">
    <!-- Start Shop Page -->
    <div class="page-shop-sidebar left--sidebar bg--white section-padding--lg">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-12 order-2 order-lg-1 md-mt-40 sm-mt-40">
                    <div class="shop__sidebar">
                        <aside class="wedget__categories poroduct--cat">
                            <h3 class="wedget__title">Категории</h3>
                            <ul>
                                <li class=@if ($books->path() == route('home'))
                                    active_category
                                @endif><a href={{ route('home') }}>Все книги</a></li>
                                @foreach($categories as $category)
									<li class=@if (str_contains($books->path(), $category->slug))
                                        active_category
                                    @endif>
                                        <a href={{ route('home') . '/' . $category->slug }}>{{ $category->name }}</a>
                                    </li>
								@endforeach
                            </ul>
                        </aside>
                    </div>
                </div>
                <div class="col-lg-9 col-12 order-1 order-lg-2">
                    <div class="tab__container">
                        <div class="row">
                            <div class="col-lg-12">
                                <form action={{ url()->full() }} method="post">
                                    @csrf
                                    <div class="shop__list__wrapper mx-auto">
                                    <div class="d-flex flex-wrap flex-md-nowrap justify-content-between">
                                      <p>Показано {{ (($books->currentPage() - 1) * 12) + 1 }}–{{ (($books->currentPage() - 1) * 12) + $books->count() }} из {{ $books->total() }} результатов</p>
                                      </div>
                                    </div>
                                </form>
                            </div>
                            @foreach ($books as $book)
                                <!-- Start Single Product -->
                                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div class="product">
                                        <div class="product__thumb">
                                            <a class="first__img" href={{ route('book', $book->id) }}><img src={{ $book->picture }} alt="product image"></a>
                                            <ul class="prize position__right__bottom d-flex">
                                                <li>{{ $book->price }} ₽</li>
                                            </ul>
                                        </div>
                                        <div class="product__content">
                                            <h4><a href={{ route('book', $book->id) }}>{{$book->name}}</a></h4>
                                            <ul class="rating d-flex">
                                                @foreach (range(1, 5) as $star)
                                                    <li class=@if ($book->rate >= $star) on @endif><i class="fa-solid fa-star"></i></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                            @endforeach
                        </div>
                        @php
                            $currentPage = $books->currentPage(); // текущая страница
                            $totalPages = $books->lastPage(); // общее количество страниц
                            $pageRange = 2; // количество страниц, которые вы хотите показать слева и справа от текущей страницы
                        @endphp

                        @if ($totalPages > 1)
                            <ul class="wn__pagination">
                                @if ($books->currentPage() > 1)
                                    <li><a href={{'?search=' . app('request')->input('search') . 'page=' . $books->currentPage() - 1 }}><i class="fa-solid fa-angle-left"></i></a></li>
                                @endif
                                @if ($currentPage - $pageRange - 1 > 1)
                                    <li><a href="?search={{ app('request')->input('search') }}&page=1">1</a></li>
                                    <li><a>...</a></li>
                                @endif

                                @for ($i = max($currentPage - $pageRange, 1); $i <= min($currentPage + $pageRange, $totalPages); $i++)
                                    <li class="{{ ($currentPage == $i) ? 'active' : '' }}"><a href="?search={{ app('request')->input('search') }}&page={{ $i }}">{{ $i }}</a></li>
                                @endfor

                                @if ($currentPage + $pageRange + 1 < $totalPages)
                                    <li><a>...</a></li>
                                    <li><a href="?search={{ app('request')->input('search') }}&page={{ $totalPages }}">{{ $totalPages }}</a></li>
                                @endif
                                
                                @if ($books->currentPage() < $books->lastPage())
                                    <li><a href="?search={{ app('request')->input('search') }}&page={{$books->currentPage() + 1}}"><i class="fa-solid fa-angle-right"></i></a></li>
                                @endif
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Shop Page -->
</section>
@endsection