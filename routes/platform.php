<?php

declare(strict_types=1);

use App\Orchid\Screens\PlatformScreen;
use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

// Main
Route::screen('/main', PlatformScreen::class)
    ->name('platform.main');

// Platform > Books
Route::screen('books', \App\Orchid\Screens\BookListScreen::class)
    ->name('platform.book.list');
Route::screen('book-create', \App\Orchid\Screens\BookCreateScreen::class)
    ->name('platform.book.create');
Route::screen('book/{id}', \App\Orchid\Screens\BookEditScreen::class)
    ->name('platform.book.edit');

// Platform > Categories
Route::screen('categories', \App\Orchid\Screens\CategoriesListScreen::class)
    ->name('platform.category.list');
Route::screen('category-create', \App\Orchid\Screens\CategoryCreateScreen::class)
    ->name('platform.category.create');
Route::screen('category/{id}', \App\Orchid\Screens\CategoryEditScreen::class)
    ->name('platform.category.edit');

// Platform > Authors
Route::screen('authors', \App\Orchid\Screens\AuthorsListScreen::class)
    ->name('platform.author.list');
Route::screen('author-create', \App\Orchid\Screens\AuthorCreateScreen::class)
    ->name('platform.author.create');
Route::screen('author/{id}', \App\Orchid\Screens\AuthorEditScreen::class)
    ->name('platform.author.edit');

// Platform > Genres
Route::screen('genres', \App\Orchid\Screens\GenreListScreen::class)
    ->name('platform.genre.list');
Route::screen('genre-create', \App\Orchid\Screens\GenreCreateScreen::class)
    ->name('platform.genre.create');
Route::screen('genre/{id}', \App\Orchid\Screens\GenreEditScreen::class)
    ->name('platform.genre.edit');

// Platform > Messages From Users
Route::screen('messages', \App\Orchid\Screens\MessageListScreen::class)
    ->name('platform.message.list');

// Platform > Profile
Route::screen('profile', \App\Orchid\Screens\User\UserProfileScreen::class)
->name('platform.profile')
->breadcrumbs(fn (Trail $trail) => $trail
    ->parent('platform.index')
    ->push(__('Profile'), route('platform.profile')));
