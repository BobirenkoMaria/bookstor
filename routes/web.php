<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Route::post('/cookie', function () {
    
})->name('accept-cookie');

Route::get('/home', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home/{slug}', [\App\Http\Controllers\HomeController::class, 'category'])->name('home-category');

Route::get('/about', [\App\Http\Controllers\AboutController::class, 'index'])->name('about');
Route::post('/contact', [\App\Http\Controllers\AboutController::class, 'store'])->name('contact');

Route::get('/book/{id}', [\App\Http\Controllers\BookController::class, 'index'])->name('book');
Route::post('/book/{id}/pre-order', [\App\Http\Controllers\OrderController::class, 'store'])->name('order-book');

Route::get('/verificated-order', [\App\Http\Controllers\PaymentController::class, 'create'])->name('verificated-order');
Route::post('/payment/create/{bookId}', [\App\Http\Controllers\PaymentController::class, 'create'])->name('payment-create');
Route::match(['GET', 'POST'], '/payment/callback', [\App\Http\Controllers\PaymentController::class, 'callback'])->name('payment-callback');


require __DIR__.'/auth.php';