import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: ['resources/css/app.css', 
            'resources/js/active.js', 'resources/js/app.js'],
            refresh: true,
            transformOnServe: (code) => code.replaceAll('/images', 'http://localhost:8000/images'),
        }),
    ],
});
