<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Book;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Redis::flushall();

        \App\Models\Author::factory(20)->create();
        \App\Models\Genre::factory(20)->create();

        DB::table('categories')->insert([
            [
                'id' => 1,
                'name' => 'Художественная литература',
                'slug' => 'art'
            ],
            [  
                'id' => 2,
                'name' => 'Научно-популярная литература',
                'slug' => 'science'
            ],
            [
                'id' => 3,
                'name' => 'Книги на английском языке',
                'slug' => 'english'
            ],
            [
                'id' => 4,
                'name' => 'Философия',
                'slug' => 'philosophy'
            ],
            [
                'id' => 5,
                'name' => 'Психология',
                'slug' => 'psychology'
            ],
            [
                'id' => 6,
                'name' => 'Детские книги',
                'slug' => 'children'
            ],
            [
                'id' => 7,
                'name' => 'Книги для подростков',
                'slug' => 'teenager'
            ],
            [
                'id' => 8,
                'name' => 'Образование',
                'slug' => 'education'
            ],
            [
                'id' => 9,
                'name' => 'Справочники',
                'slug' => 'reference'
            ],
            [
                'id' => 10,
                'name' => 'Наука. Техника. IT',
                'slug' => 'it'
            ]
        ]);
        
        if (!Storage::exists('public/files/lorem_ipsum.docx')) {
            Storage::putFileAs('public/files', public_path('/files/lorem_ipsum.docx'), 'lorem_ipsum');
        }
        DB::table('attachments')->insert([
            [
                'id' => 1,
                'name' => 'lorem_ipsum',
                'original_name' => 'lorem_ipsum.docx',
                'mime' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'path' => 'files',
                'extension' => 'docx',
            ],
        ]);

        Book::factory(1000)->create();

        $books = Book::all();
        $genres = \App\Models\Genre::all();
        $book_genre = [];
        $id = 1;
        foreach ($books as $book) {
            $countForBook = rand(1, 5);
            foreach (range(1, $countForBook) as $i) {
                $book_genre[] = [
                    'id' => $id++,
                    'book_id' => $book->id,
                    'genre_id' => $genres->random()->id
                ];
            }
        }
        DB::table('book_genre')->insert($book_genre);

        $authors = \App\Models\Author::all();
        $author_book = [];
        $id = 1;
        foreach ($books as $book) {
            $countForBook = rand(1, 5);
            foreach (range(1, $countForBook) as $i) {
                $author_book[] = [
                    'id' => $id++,
                    'book_id' => $book->id,
                    'author_id' => $authors->random()->id
                ];
            }
        }
        DB::table('author_book')->insert($author_book);

        Artisan::call('orchid:admin admin admin@admin.com admin');
    }
}
