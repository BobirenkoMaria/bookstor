<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Book>
 */
class BookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'picture' => function () {
                $randPic = rand(1, 8);

                $picUrl = '/images/product/1.jpg';
                switch ($randPic) {
                    case 1:
                        $picUrl = '/images/product/blacksun.jpg';
                        break;
                    case 2:
                        $picUrl = '/images/product/nightship.jpg';
                        break;
                    case 3:
                        $picUrl = '/images/product/blades.jpg';
                        break;
                    case 4:
                        $picUrl = '/images/product/mind.png';
                        break;
                    case 5:
                        $picUrl = '/images/product/marketing.jpg';
                        break;
                    case 6:
                        $picUrl = '/images/product/machinesoul.png';
                        break;
                    case 7:
                        $picUrl = '/images/product/perfectcoder.jpg';
                        break;
                    case 8:
                        $picUrl = '/images/product/pofig.jpg';
                        break;
                };
                return $picUrl;
            },
            // 'picture' => '/images/product/1.jpg',
            'name' => fake()->name(),
            'description' => fake()->text(),
            'price' => fake()->numberBetween(100, 1000),
            'rate' => fake()->numberBetween(1, 5),
            'category_id' => \App\Models\Category::all()->random()->id,
            'attachment_id' => \Orchid\Attachment\Models\Attachment::all()->random()->id
        ];
    }
}
