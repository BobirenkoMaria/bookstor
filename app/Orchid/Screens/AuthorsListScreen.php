<?php

namespace App\Orchid\Screens;

use App\Models\Author;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Toast;

class AuthorsListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Список авторов';
    public $authors;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'authors' => Author::paginate(12),
        ];
    }

    public function commandBar(): array
    {
        return [
            Link::make(__('Добавить'))
                ->icon('bs.plus-circle')
                ->route('platform.author.create'),
        ];
    }

    /**
     * Display data.
     *
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): array
    {
        return [
            \App\Orchid\Layouts\AuthorListLayout::class,
        ];
    }

    /**
     * Remove book action.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove(Request $request): void
    {
        Author::findOrFail($request->get('id'))->delete();
        Toast::info('Автор успешно удален');
    }
}
