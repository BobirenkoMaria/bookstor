<?php

namespace App\Orchid\Screens;

use App\Models\Category;
use App\Services\CategoriesService;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Illuminate\Http\Request;
use Orchid\Support\Facades\Toast;

class CategoriesListScreen extends Screen
{
    private $categoryService;

    public function __construct(){
        $this->categoryService = new CategoriesService();
    }
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Список категорий';
    public $categories;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'categories' => Category::paginate(12),
        ];
    }

    public function commandBar(): array
    {
        return [
            Link::make(__('Добавить'))
                ->icon('bs.plus-circle')
                ->route('platform.category.create'),
        ];
    }

    /**
     * Display data.
     *
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): array
    {
        return [
            \App\Orchid\Layouts\CategoriesListLayout::class,
        ];
    }

    /**
     * Remove book action.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove(Request $request): void
    {
        $this->categoryService->deleteCategory($request->get('id'));
        Toast::info('Категория успешно удалена');
    }
}
