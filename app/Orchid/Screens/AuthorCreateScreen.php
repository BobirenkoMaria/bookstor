<?php

namespace App\Orchid\Screens;

use App\Models\Author;
use Orchid\Screen\Screen;
use Illuminate\Http\Request;
use Orchid\Support\Facades\Layout;
use App\Orchid\Layouts\AuthorCreateLayout;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Color;

class AuthorCreateScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Создание Автора';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    public function layout(): iterable
    {
        return [
            Layout::block(AuthorCreateLayout::class)
                ->title('Добавить книгу')
                ->commands(
                    Button::make('Сохранить')
                        ->type(Color::BASIC)
                        ->icon('bs.check-circle')
                        ->method('save')
                )
        ];
    }

    public function save(Author $author, Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string',
        ]);
        
        $author->create($validated);

        return redirect()->route('platform.author.list');
    }
}
