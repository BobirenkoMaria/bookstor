<?php

namespace App\Orchid\Screens;

use App\Http\Requests\BookCreateRequest;
use App\Models\Book;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use App\Orchid\Layouts\BookCreateLayout;
use App\Services\BookService;
use Orchid\Attachment\File;
use Orchid\Attachment\Models\Attachment;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Color;

class BookCreateScreen extends Screen
{
    private $bookService;
    public function __construct()
    {
        $this->bookService = new BookService();
    }
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Добавить книгу';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::block(BookCreateLayout::class)
                ->title('Добавить книгу')
                ->commands(
                    Button::make('Сохранить')
                        ->type(Color::BASIC)
                        ->icon('bs.check-circle')
                        ->method('save')
                )
        ];
    }

    public function save(BookCreateRequest $request)
    {
        $this->bookService->addBook($request->validated());

        return redirect()->route('platform.book.list');
    }
}
