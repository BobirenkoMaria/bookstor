<?php

declare(strict_types=1);

namespace App\Orchid\Screens;

use App\Models\Book;
use App\Orchid\Layouts\BookCreateLayout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;

class PlatformScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [];
    }

    /**
     * The name of the screen displayed in the header.
     */
    public function name(): ?string
    {
        return 'Административная панель';
    }

    /**
     * Display header description.
     */
    public function description(): ?string
    {
        return 'Добро пожаловать в административную панель интернет-магазина BookStor! 
        Здесь вы можете легко управлять книгами, авторами, категориями и страницами, добавлять, редактировать, удалять и скрывать контент для обеспечения актуальности и удобства использования сайта.';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [];
    }
}
