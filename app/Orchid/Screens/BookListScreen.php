<?php

namespace App\Orchid\Screens;

use App\Models\Book;
use App\Services\BookService;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\TD;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Picture;
use Illuminate\Http\Request;
use Orchid\Support\Facades\Toast;

class BookListScreen extends Screen
{
    private $bookService;
    public function __construct()
    {
        $this->bookService = new BookService();
    }
    
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Список книг';
    public $books;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'books' => Book::paginate(12),
        ];
    }

    public function commandBar(): array
    {
        return [
            Link::make(__('Добавить'))
                ->icon('bs.plus-circle')
                ->route('platform.book.create'),
        ];
    }

    /**
     * Display data.
     *
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): array
    {
        return [
            \App\Orchid\Layouts\BookListLayout::class,
        ];
    }

    /**
     * Remove book action.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove(Request $request): void
    {
        $this->bookService->deleteBook($request->get('id'));
        Toast::info('Книга успешно удалена');
    }

    public function setShow(Request $request) : void
    {
        $this->bookService->setInShow($request->get('id'));
    }
}
