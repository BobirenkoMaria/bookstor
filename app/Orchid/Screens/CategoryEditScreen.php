<?php

namespace App\Orchid\Screens;

use App\Services\CategoriesService;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Color;
use Orchid\Support\Facades\Toast;
use Orchid\Screen\Actions\Button;
use App\Orchid\Layouts\CategoryEditLayout;
use Illuminate\Http\Request;

class CategoryEditScreen extends Screen
{
    private $categoryService;
    public $category;

    public function __construct()
    {
        $this->categoryService = new CategoriesService();
    }

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query($id): iterable
    {
        return [
            'category' => $this->categoryService->getCategory($id),
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Изменить Категорию';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::block(CategoryEditLayout::class)
                ->title('Изменить Категорию')
                ->commands(
                    Button::make('Сохранить')
                        ->type(Color::BASIC)
                        ->icon('bs.check-circle')
                        ->method('edit')
                )
        ];
    }

    public function edit(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'slug' => 'required|string',
        ]);
        $this->categoryService->updateCategory($this->category->id, $validated);
    }
}
