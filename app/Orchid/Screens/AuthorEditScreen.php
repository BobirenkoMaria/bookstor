<?php

namespace App\Orchid\Screens;

use App\Models\Author;
use App\Orchid\Layouts\AuthorEditLayout;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Color;
use Orchid\Support\Facades\Toast;

class AuthorEditScreen extends Screen
{
    public $author;
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query($id): iterable
    {
        return [
            'author' => Author::findOrFail($id),
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Изменить Автора';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::block(AuthorEditLayout::class)
                ->title('Изменить Автора')
                ->commands(
                    Button::make('Сохранить')
                        ->type(Color::BASIC)
                        ->icon('bs.check-circle')
                        ->method('edit')
                )
        ];
    }

    public function save(\Illuminate\Http\Request $request, $id)
    {
        $author = Author::findOrFail($id);
        $author->name = $request->input('author.name');
        $author->save();

        Toast::info('Автор успешно обновлен');

        return redirect()->route('platform.author.list');
    }
}
