<?php

namespace App\Orchid\Screens;

use App\Models\Category;
use Orchid\Screen\Screen;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Orchid\Support\Facades\Layout;
use App\Orchid\Layouts\CategoriesCreateLayout;
use App\Services\CategoriesService;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Color;

class CategoryCreateScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Создание категории';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::block(CategoriesCreateLayout::class)
                ->title('Добавить книгу')
                ->commands(
                    Button::make('Сохранить')
                        ->type(Color::BASIC)
                        ->icon('bs.check-circle')
                        ->method('save')
                )
        ];
    }

    public function save(CategoriesService $categoriesService, Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'slug' => 'string',
        ]);
        
        if (!isset($validated['slug'])) {
            $validated['slug'] = Str::slug($validated['slug']);
        }
        $categoriesService->addCategory($validated);

        return redirect()->route('platform.category.list');
    }
}
