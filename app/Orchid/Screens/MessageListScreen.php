<?php

namespace App\Orchid\Screens;

use App\Models\Contact;
use Illuminate\Http\Request;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Toast;
use App\Orchid\Layouts\MessageListLayout;

class MessageListScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'contacts' => Contact::paginate(12)
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Обращения пользователей';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            MessageListLayout::class,
        ];
    }

    public function setStatus(Request $request) : void
    {
        $contact = Contact::find($request->get('id'));
        $contact->update(['status' => $contact->status ? false : true]);
    }

    public function remove(Request $request): void
    {
        Contact::findOrFail($request->get('id'))->delete();
        Toast::info('Сообщение успешно удалено');
    }
}
