<?php

namespace App\Orchid\Screens;

use App\Http\Requests\BookCreateRequest;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Color;
use Orchid\Screen\Actions\Button;
use App\Orchid\Layouts\BookEditLayout;
use App\Services\BookService;

class BookEditScreen extends Screen
{
    private $bookService;
    public $book;

    public function __construct()
    {
        $this->bookService = new BookService();
    }

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query($id): iterable
    {
        $book = $this->bookService->getBookById($id);
        $book['genres'] = $book->genres->pluck('id')->toArray();
        $book['file'] = [$book['attachment_id']];
        
        $this->book = $book;

        return [
            'book' => $book,
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Изменить Книгу';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::block(BookEditLayout::class)
                ->title('Изменить книгу')
                ->commands(
                    Button::make('Сохранить')
                        ->type(Color::BASIC)
                        ->icon('bs.check-circle')
                        ->method('edit')
                )
        ];
    }

    public function edit(BookCreateRequest $request)
    {
        $this->bookService->updateBook($this->book->id, $request->validated());

        return redirect()->route('platform.book.list');
    }
}
