<?php

namespace App\Orchid\Screens;

use App\Models\Genre;
use Orchid\Screen\Screen;
use Illuminate\Http\Request;
use Orchid\Support\Facades\Layout;
use App\Orchid\Layouts\GenreCreateLayout;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Color;

class GenreCreateScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Создание Жанра';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    public function layout(): iterable
    {
        return [
            Layout::block(GenreCreateLayout::class)
                ->title('Добавить книгу')
                ->commands(
                    Button::make('Сохранить')
                        ->type(Color::BASIC)
                        ->icon('bs.check-circle')
                        ->method('save')
                )
        ];
    }

    public function save(Genre $genre, Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string',
        ]);
        $genre->create($validated);

        return redirect()->route('platform.genre.list');
    }
}
