<?php

namespace App\Orchid\Screens;

use App\Models\Author;
use App\Models\Genre;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Toast;

class GenreListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Список жанров';
    public $genres;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'genres' => Genre::paginate(12),
        ];
    }

    public function commandBar(): array
    {
        return [
            Link::make(__('Добавить'))
                ->icon('bs.plus-circle')
                ->route('platform.genre.create'),
        ];
    }

    /**
     * Display data.
     *
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): array
    {
        return [
            \App\Orchid\Layouts\GenreListLayout::class,
        ];
    }

    /**
     * Remove book action.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove(Request $request): void
    {
        Genre::findOrFail($request->get('id'))->delete();
        Toast::info('Жанр успешно удален');
    }
}
