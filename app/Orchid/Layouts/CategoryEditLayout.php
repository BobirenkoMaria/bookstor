<?php 

namespace App\Orchid\Layouts;
use Orchid\Screen\Layouts\Rows;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Select;

class CategoryEditLayout extends Rows
{
    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): iterable
    {
        return [
            Input::make('category.name')
                ->value($this->query->get('category.name'))
                ->title('Имя')
                ->type('text')
                ->max(100)
                ->required()
                ->name('name'),
            Input::make('category.slug')
                ->value($this->query->get('category.slug'))
                ->title('Slug')
                ->type('text')
                ->max(100)
                ->required()
                ->name('slug'),
        ];
    }
}