<?php 

namespace App\Orchid\Layouts;
use Orchid\Screen\Layouts\Rows;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Select;

class AuthorEditLayout extends Rows
{
    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): iterable
    {
        return [
            Input::make('author.name')
                ->value($this->query->get('author.name'))
                ->title('Имя')
                ->type('text')
                ->max(50)
                ->required()
                ->name('name'),
        ];
    }
}