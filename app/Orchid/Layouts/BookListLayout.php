<?php

namespace App\Orchid\Layouts;

use App\Models\Book;
use Illuminate\Support\Facades\Storage;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class BookListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'books';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('picture', 'Изображение')
                ->render(function (Book $book) {
                    if($book->picture) { return '<img src="'.$book->picture.'" height="100px" alt>'; } return null;
                })
                ->width('100px'),
            TD::make('name', 'Название')
                ->width('200px'),
            TD::make('description', 'Описание')
                ->width('400px'),
            TD::make('in_show', 'В показ')
                ->render(fn (Book $book) =>
                    Button::make($book->in_show ? 'Снять с показа' : 'Восстановить в показ')
                        ->method('setShow', [
                            'id' => $book->id,
                        ])
                ),

            TD::make('Действия')
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(fn (Book $book) => DropDown::make()
                    ->icon('bs.three-dots-vertical')
                    ->list([
                        Link::make('Редактировать')
                            ->icon('bs.pencil')
                            ->route('platform.book.edit', $book->id),
                        Button::make('Удалить')
                            ->icon('bs.trash3')
                            ->confirm('Вы уверены?')
                            ->method('remove', [
                                'id' => $book->id,
                            ])
                    ])),
        ];
    }
}
