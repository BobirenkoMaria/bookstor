<?php 

namespace App\Orchid\Layouts;

use App\Models\Book;
use App\Models\Author;
use App\Models\Category;
use App\Models\Genre;
use Orchid\Screen\Layouts\Rows;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Select;

class BookEditLayout extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): iterable
    {
        return [
            Picture::make('book.picture')
                ->value($this->query->get('book.picture'))
                ->title('Изображение')
                ->required()
                ->name('picture'),

            Input::make('book.name')
            ->value($this->query->get('book.name'))
                ->title('Название')
                ->type('text')
                ->max(100)
                ->required()
                ->name('name'),

            Input::make('book.description')
            ->value($this->query->get('book.description'))
                ->title('Описание')
                ->type('text')
                ->max(255)
                ->vertical()
                ->required()
                ->name('description'),

            Input::make('book.rate')
            ->value($this->query->get('book.rate'))
                ->min(0)
                ->max(5)
                ->title('Рейтинг')
                ->type('number')
                ->required()
                ->name('rate'),

            Select::make('book.category_id')
            ->value($this->query->get('book.category_id'))
                ->fromModel(Category::class, 'name')
                ->title('Категория')
                ->name('category_id'),

            Input::make('book.price')
            ->min(0)
            ->value($this->query->get('book.price'))
                ->title('Цена')
                ->type('number')
                ->required()
                ->name('price'),

            Relation::make('book.authors')
                ->multiple()
                ->value($this->query->get('book.authors'))
                ->title('Автор')
                ->fromModel(Author::class, 'name')
                ->required()
                ->name('authors'),

            Relation::make('book.genres')
            ->value($this->query->get('book.genres'))
                ->title('Жанры')
                ->multiple()
                ->fromModel(Genre::class, 'name')
                ->required()
                ->name('genres'),

            CheckBox::make('book.in_show')
            ->value($this->query->get('book.in_show'))
                ->title('Выводить на сайте')
                ->sendTrueOrFalse()
                ->name('in_show'),

            Upload::make('book.attachment_id')
            ->value($this->query->get('book.file'))
                ->maxFiles(1)
                ->title('Книга')
                ->required()
                ->name('attachment_id'),
        ];
    }
}