<?php

namespace App\Orchid\Layouts;

use App\Models\Book;
use App\Models\Contact;
use Illuminate\Support\Facades\Storage;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class MessageListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'contacts';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('id', 'ID'),

            TD::make('name', 'Имя пользователя')
                ->width('200px'),
            TD::make('email', 'Email пользователя')
                ->width('200px'),

            TD::make('message', 'Обращение')
                ->width('400px'),

            TD::make('status', 'Обработка запросов')
                ->render(fn (Contact $contact) =>
                    Button::make($contact->status ? 'Обработано' : 'На рассмотрении')
                        ->method('setStatus', [
                            'id' => $contact->id,
                        ])
                ),

            TD::make('Действия')
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(fn (Contact $contact) => DropDown::make()
                    ->icon('bs.three-dots-vertical')
                    ->list([
                        Button::make('Удалить')
                            ->icon('bs.trash3')
                            ->confirm('Вы уверены?')
                            ->method('remove', [
                                'id' => $contact->id,
                            ])
                    ])),
        ];
    }
}
