<?php 

namespace App\Services;

use Illuminate\Database\Eloquent\Collection;

class CollectionService
{
    public function convertArrayToCollection($array, $model) {
        $collection = new Collection();
        foreach ($array as $element) {
            $model = new $model;
            $model = $model->newInstance($element, true);

            $collection->push($model);
        }

        return $collection;
    }
}