<?php 

namespace App\Services;

use App\Models\Author;
use App\Services\RedisService;

class AuthorService
{
    private $redisService;
    public function __construct(){
        $this->redisService = new RedisService();
    }

    public function addAuthor($params) {
        $this->redisService->addModelToHash($params, Author::class, 'authors');
    }
}