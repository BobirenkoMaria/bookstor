<?php 

namespace App\Services;

use App\Models\Category;

class CategoriesService
{
    private $redisService;

    public function __construct() {
        $this->redisService = new RedisService();
    }

    public function getCategories() {
        $categories = $this->redisService->getModelCollection(Category::class, Category::MODEL_NAME);
        return $categories;
    }

    public function addCategory($params) {
        $this->redisService->addModelToHash($params, Category::class, Category::MODEL_NAME);
    }

    public function getCategory($id) {
        $category = $this->redisService->getModelById($id, Category::class, Category::MODEL_NAME);
        return $category;
    }

    public function updateCategory($id, $params) {
        $params['id'] = $id;
        $this->redisService->updateModelInHash($params, Category::class, Category::MODEL_NAME);
    }

    public function deleteCategory($id) {
        $this->redisService->deleteModelById($id, Category::class, Category::MODEL_NAME);
    }
}