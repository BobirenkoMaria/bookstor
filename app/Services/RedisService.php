<?php 

namespace App\Services;

use Illuminate\Support\Facades\Redis;

class RedisService
{
    private $collectionService;

    public function __construct() {
        $this->collectionService = new CollectionService();
    }

    public function getModelById($id, $model, $modelName) {
        $redisElem = Redis::hGet($modelName, $id);

        if(isset($elem)) {
            $modelElem = new $model();
            $elem = $modelElem->newInstance(json_decode($redisElem, false), true);
        }
        else {
            $elem = $model::findOrFail($id);
            Redis::hSet($modelName, $id, json_encode($elem));
        }

        return $elem;
    }

    public function addModelToHash($params, $model, $modelName, $hasManyToManyRelationship = false) {
        $model = new $model;
        $model = $model->create($params);
        Redis::hSet($modelName, $model->id, json_encode($model));

        return $model;
    }

    public function addManyToManyModelToHash($params, $modelName) {
        Redis::hSet($modelName, $params['id'], json_encode($params));
    }

    public function updateModelInHash($params, $model, $modelName) {
        if(Redis::hExists($modelName, $params['id'])) {
            Redis::hDel($modelName, $params['id']);
        }
        Redis::hSet($modelName, $params['id'], json_encode($model));
    }

    public function deleteModelById($id, $model, $modelName) {
        $model = new $model;
        $model = $model::findOrFail($id);
        return Redis::hDel($modelName, $id);
    }

    public function getModelCollection($model, $modelName) {
        $redisModel = Redis::hVals($modelName);

        $modelCollection = [];
        if ($redisModel != []){
            $modelList = [];
            foreach ($redisModel as $key => $value) {
                $modelList[$key] = json_decode($value, true);
            }
            $modelCollection = $this->collectionService->convertArrayToCollection($modelList, $model);
        }
        else {
            $model = new $model;
            $modelCollection = $model->all();

            foreach ($modelCollection as $elem) {
                Redis::hSet($modelName, $elem->id, json_encode($elem));
            }
        }

        return $modelCollection;
    }

    public function isEmailWereSent($email, $bookId, $isVarification = false) {
        $emailType = ($isVarification) ? ('varification_email') : ('successful_email');
        $redisEmail = Redis::hGet($emailType, $email.'_'.$bookId);

        return (isset($redisEmail)) ? true : false;
    }

    public function setEmailSent($email, $bookId, $isVarification = false) {
        $emailType = ($isVarification) ? ('varification_email') : ('successful_email');
        Redis::hSet($emailType, $email.'_'.$bookId, json_encode([
            'email' => $email,
            'bookId' => $bookId
        ]));

        $caching_hours = 1;
        Redis::expire($emailType . 'email_bookId:'.$email.'_'.$bookId, $caching_hours * 3600);
    }

    public function hasInRedis($hashName, $name) {
        return Redis::hGet($hashName, $name);
    }

    public function setInRedis($hashName, $name, $value) {
        Redis::hSet($hashName, $name, json_encode($value));
    }
}