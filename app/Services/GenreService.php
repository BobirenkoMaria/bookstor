<?php 

namespace App\Services;

use App\Models\Genre;

class GenreService
{
    private $redisService;
    public function __construct()
    {
        $this->redisService = new RedisService;
    }

    public function addGenre($params)
    {
        $this->redisService->addModelToHash($params, Genre::class, 'genre');
    }
}