<?php 

namespace App\Services;

use App\Models\Transaction;
use YooKassa\Client;

class PaymentService
{
    private $redisService, $bookService;
    public function __construct()
    {
        $this->redisService = new RedisService();
        $this->bookService = new BookService();
    }

    public function getClient() : Client
    {
        $client = new Client();
        $client->setAuth(config('services.yookassa.shop_id'), config('services.yookassa.secret_key'));
        return $client;
    }

    public function createPayment(float $amount, string $description, array $options = [])
    {
        $idempotenceKey = uniqid('', true);

        $client = $this->getClient();
        $response = $client->createPayment(
        array(
            'amount' => array(
                'value' => $amount,
                'currency' => 'RUB',
            ),
            'capture' => false,
            'payment_method_data' => array(
                'type' => 'bank_card',
            ),
            'confirmation' => array(
                'type' => 'redirect',
                'return_url' => route('book', [
                    'id' => $options['book_id'],
                ]),
            ),
            'description' => $description,
            'metadata' => [
                'transaction_id' => $options['transaction_id'],
            ]
        ),
        $idempotenceKey
        );

        //get confirmation url
        return $response->getConfirmation()->getConfirmationUrl();
    }
}