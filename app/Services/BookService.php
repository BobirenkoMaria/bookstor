<?php

namespace App\Services;

use App\Models\Author;
use App\Models\Book;
use App\Models\Genre;
use Orchid\Attachment\Models\Attachment;

class BookService
{
    private $redisService;

    public function __construct()
    {
        $this->redisService = new RedisService;
    }
    
    public function getBookById($id)
    {
        $book = $this->redisService->getModelById($id, Book::class, Book::MODEL_NAME);
        return $book;
    }

    public function addBook($params)
    {
        $book = new Book();
        $params['attachment_id'] = ($params['attachment_id'])[0];

        $authorsId = $params['authors'];
        $genresId = $params['genres'];

        $book = $book->create($params);
        
        $authors = Author::find($authorsId);
        $book->authors()->attach($authors);

        $genres = Genre::find($genresId);
        $book->genres()->attach($genres);

        return $this->redisService->addManyToManyModelToHash($book, Book::MODEL_NAME);
    }

    public function deleteBook($id)
    {
        $this->redisService->deleteModelById($id, Book::class, Book::MODEL_NAME);
    }

    public function updateBook($id, $params)
    {
        $book = Book::find($id);
        $params['attachment_id'] = ($params['attachment_id'])[0];

        $authorsId = $params['authors'];
        $genresId = $params['genres'];
        
        $book->update($params);
        $book->save();
        
        $authors = Author::find($authorsId);
        $book->authors()->sync($authors);
        
        $genres = Genre::find($genresId);
        $book->genres()->sync($genres);

        $this->redisService->updateModelInHash($book, Book::class, Book::MODEL_NAME);
    }

    public function setInShow($bookId){
        $book = Book::find($bookId);
        $book->update(['in_show' => $book->in_show ? false : true]);
        $this->redisService->updateModelInHash($book, Book::class, Book::MODEL_NAME);
    }

    public function getBooksInShow($category = null, $page = 1)
    {
        $books = new Book();
        if (isset($category)) {
            $books = $books->where('category_id', $category->id);
        }

        return $books->where('in_show', true)->paginate(12, ['*'], 'page', $page);
    }
}