<?php 

namespace App\Services;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

class EmailService
{
    private $redisService;

    public function __construct() {
        $this->redisService = new RedisService();
    }

    public function sendVerificationEmail($email, $bookId) {
        if (!$this->redisService->isEmailWereSent($email, $bookId, true)) {
            $this->verificationMail($bookId, $email);
        }
        else {
            $this->redisService->setEmailSent($email, $bookId, true);
        }
    }

    public function verificationMail($bookId, $email) {
        $verificationLink = URL::temporarySignedRoute(
            'verificated-order',
            now()->addHours(1),
            ['email' => $email, 'bookId' => $bookId] 
        );
        Mail::to($email)->send(new \App\Mail\OrderMail($verificationLink));
    }

    public function sendBook($email, $book) {
        if (!$this->redisService->isEmailWereSent($email, $book->id)) {
            Mail::to($email)->send(new \App\Mail\BookMail($book));
        }
        else {
            $this->redisService->setEmailSent($email, $book->id);
        }
    }
}