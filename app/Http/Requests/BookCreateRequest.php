<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'picture' => 'required',
            'name' => 'required',
            'description' => 'required',
            'authors' => 'required',
            'price' => 'required|integer|min:0',
            'category_id' => 'int',
            'in_show' => 'boolean',
            'genres' => 'required',
            'attachment_id' => 'required',
        ];
    }
}
