<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Category;
use App\Services\CategoriesService;
use App\Services\BookService;

class HomeController extends Controller
{
    private $categoriesService, $bookService;

    public function __construct() {
        $this->categoriesService = new CategoriesService();
        $this->bookService = new BookService();
    }
    
    public function index(Request $request) {
        
        $categories = $this->categoriesService->getCategories();

        if(isset($request->search)) {
            $searchBooks = ($this->search($request));
            $books = $searchBooks->paginate(12);
        }
        else
        {
            $books = $this->bookService->getBooksInShow(null, $request->page);
        }

        return view('home', [
            'categories' => $categories,
            'books' => $books
        ]);
    }

    public function category($slug, Request $request) {
        $categories = $this->categoriesService->getCategories();
        $category = Category::where('slug', $slug)->first();
        $books = $this->bookService->getBooksInShow($category, (isset($request->page) ? $request->page : 1));
        return view('home', [
            'categories' => $categories,
            'books' => $books
        ]);
    }

    public function search(Request $request) {
        $books = [];
        if (isset($request->search)) {
            $search = $request->search;
            $books = Book::where('name', 'like', "%$search%");
        }
        return $books;
    }

    public function sort(Request $request) {
        $sort = $request->sort;

        if(isset($request->search)) {
            $books = ($this->search($request));
        }
        else
        {
            $books = $this->bookService->getBooksInShow($request->page);
        }

        $sortedBooks = $books->sortBy($sort, SORT_NATURAL | SORT_FLAG_CASE);
        dd($sortedBooks);
        return view('home', [
            'categories' => $this->categoriesService->getCategories(),
            'sort' => $sort,
            'books' => $sortedBooks->paginate(12)
        ]);
    }
}
