<?php

namespace App\Http\Controllers;

use App\Services\BookService;

class BookController extends Controller
{
    private $bookService;
    public function __construct()
    {
        $this->bookService = new BookService();
    }

    public function index($id)
    {
        $book = $this->bookService->getBookById($id);
        
        return view('book', [
            'book' => $book,
        ]);
    }
}
