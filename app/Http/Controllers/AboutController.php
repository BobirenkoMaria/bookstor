<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Models\Contact;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {
        return view('about');
    }

    public function store(ContactRequest $request)
    {
        $validated = $request->validated();
        Contact::create($validated);

        return redirect()->back()->with('success', 'Сообщение успешно отправлено');
    }
}
