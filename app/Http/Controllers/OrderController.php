<?php

namespace App\Http\Controllers;

use App\Services\BookService;
use App\Services\EmailService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

class OrderController extends Controller
{
    private $bookService, $emailService;

    public function __construct()
    {
        $this->bookService = new BookService();
        $this->emailService = new EmailService();
    }

    public function index(Request $request)
    {
        if (!URL::hasValidSignature($request)) {
            return view('errors.403', [
                'message' => 'Ссылка недействительна'
            ]);
        }
        
        $book = $this->bookService->getBookById($request->bookId);
        
        return view('order', [
            'book' => $book, 
            'email' => $request->email
        ]);
    }

    public function store($id, Request $request)
    {
        $book = $this->bookService->getBookById($id);
        $email = $request->email;

        $this->emailService->sendVerificationEmail($email, $id);

        return view('pre-order', 
        [
            'book' => $book,
            'email' => $email,
        ]);
    }

    public function send(Request $request){
        $book = $this->bookService->getBookById($request->id);
        $this->emailService->sendBook($request->email, $book);

        return view('success-order', [
           'book' => $book
        ]);
    }
}
