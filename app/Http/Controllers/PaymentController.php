<?php

namespace App\Http\Controllers;

use App\Enums\PaymentStatusEnum;
use App\Models\Transaction;
use App\Services\BookService;
use App\Services\PaymentService;
use App\Services\EmailService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use YooKassa\Model\Notification\NotificationEventType;
use YooKassa\Model\Notification\NotificationSucceeded;
use YooKassa\Model\Notification\NotificationWaitingForCapture;

class PaymentController extends Controller
{
    private $bookService, $paymentService, $emailService;
    public function __construct()
    {
        $this->bookService = new BookService();
        $this->paymentService = new PaymentService();
        $this->emailService = new EmailService();
    }

    public function callback(Request $request)
    {
        $source = file_get_contents('php://input');
        $requestBody = json_decode($source, true);
        Log::info($requestBody);
        $notification = ($requestBody['event'] === NotificationEventType::PAYMENT_SUCCEEDED)
            ? new NotificationSucceeded($requestBody)
            : new NotificationWaitingForCapture($requestBody);
        $payment = $notification->getObject();

        if(isset($payment->status) && $payment->status === 'waiting_for_capture') {
            $this->paymentService->getClient()->capturePayment([
                'amount' => $payment->amount,
            ], $payment->id, uniqid('', true));
        }

        if(isset($payment->status) && $payment->status === 'succeeded') {
            if((bool)$payment->paid === true) {
                $metadata = (object)$payment->metadata;
                if(isset($metadata->transaction_id)){
                    $transaction = Transaction::find($metadata->transaction_id);
                    $transaction->status = PaymentStatusEnum::CONFIRMED;
                    $transaction->save();

                    $book = $this->bookService->getBookById($transaction->book_id);
                    $this->emailService->sendBook($transaction->email, $book);
                }
            }
        }
    }

    public function create(Request $request) 
    {
        $book = $this->bookService->getBookById($request->bookId);
        $amount = (float)$book->price;
        $description = "Оплата книги " . $book->name . ' для ' . $request->email;

        $transaction = Transaction::create([
            'book_id' => $request->bookId,
            'email' => $request->email,
            'amount' => $amount,
            'description' => $description
        ]);

        if($transaction) {
            $link = $this->paymentService->createPayment($amount, $description, [
                'transaction_id' => $transaction->id,
                'book_id' => $request->bookId,
                'email' => $request->email
            ]);

            return redirect()->away($link);
        }

        $this->paymentService->createPayment($amount, $description, [
            'transaction_id' => $transaction->id,
            'book_id' => $request->bookId,
            'email' => $request->email
        ]);
    }

    public function send($id){
        $book = $this->bookService->getBookById($id);

        return view('success-order', [
           'book' => $book
        ]);
    }
}
