<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Orchid\Attachment\Attachable;
use Orchid\Attachment\Models\Attachment;
use Orchid\Screen\AsSource;

class Book extends Model
{
    use HasFactory, AsSource, Attachable;

    public const MODEL_NAME = 'book';

    protected $fillable = [
        'id',
        'picture',
        'name',
        'description',
        'price',
        'rate',
        'category_id',
        'in_show',
        'attachment_id', 
        'file'
    ];

    public function authors()
    {
        return $this->belongsToMany(Author::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function genres()
    {
        return $this->belongsToMany(Genre::class);
    }

    public function attachment()
    {
        return $this->belongsTo(Attachment::class);
    }
}
