<?php

namespace App\Enums;

class PaymentStatusEnum
{
    public const CREATE = 'create';
    public const FAILED = 'failed';
    public const CONFIRMED = 'confirmed';
}